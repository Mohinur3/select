with StaffRevenue as (
  select
    s.store_id,
    p.staff_id,
    sum(p.amount) as total_revenue
  from
    payment p
    join staff s on p.staff_id = s.staff_id
    join rental r on p.rental_id = r.rental_id
    join inventory i on r.inventory_id = i.inventory_id
    join store st on i.store_id = st.store_id
  where
    extract(year from p.payment_date) = 2017
  group by
    s.store_id, p.staff_id
)
, RankedStaff as (
  select
    store_id,
    staff_id,
    total_revenue,
    RANK() over (partition by store_id order by total_revenue desc) as rank
  from
    StaffRevenue
)
select
  rs.store_id,
  rs.staff_id,
  s.first_name,
  s.last_name,
  rs.total_revenue
from
  RankedStaff rs
  join staff s on rs.staff_id = s.staff_id
where
  rs.rank = 1;

select
    a.actor_id,
    concat(a.first_name, ' ', a.last_name) as actor_name,
    max(f.release_year) as last_film_year,
    2023 - max(f.release_year) as years_without_acting
from
    actor a
join
    film_actor fa on a.actor_id = fa.actor_id
join
    film f on fa.film_id = f.film_id
group by
    a.actor_id, actor_name
order by
    years_without_acting desc
limit 1

with TopRentedMovies as (
    select
        f.film_id,
        f.title as film_title,
        count(rental_id) as rental_count
    from
        film f
    join inventory i on f.film_id = i.film_id
    join rental r on i.inventory_id = r.inventory_id
    group by
        f.film_id, f.title
    order by
        rental_count desc
    limit 5
)
select
    trm.film_title,
    f.rating as film_rating
FROM
    TopRentedMovies trm
JOIN film f ON trm.film_id = f.film_id;